package ca.cellularautomata.core.processor.entity;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Generated;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JConditional;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JExpr;
import com.sun.codemodel.JExpression;
import com.sun.codemodel.JFieldVar;
import com.sun.codemodel.JInvocation;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JType;
import com.sun.codemodel.JVar;

import ca.cellularautomata.core.shared.entity.AbstractEntity;
import ca.cellularautomata.core.shared.entity.AbstractEntityFactory;
import ca.cellularautomata.core.shared.entity.AbstractEntityFactory.EntityInstantiator;
import ca.cellularautomata.core.shared.entity.EntityFactory;
import ca.cellularautomata.core.shared.entity.EntityMeta;
import ca.cellularautomata.core.shared.entity.IEntity;
import ca.cellularautomata.core.shared.entity.IList;
import ca.cellularautomata.core.shared.entity.IObject;
import ca.cellularautomata.core.shared.entity.IPrimitive;
import ca.cellularautomata.core.shared.entity.ISet;
import ca.cellularautomata.core.shared.entity.ListImpl;
import ca.cellularautomata.core.shared.entity.Path;
import ca.cellularautomata.core.shared.entity.PrimitiveImpl;
import ca.cellularautomata.core.shared.entity.SetImpl;
import ca.cellularautomata.core.shared.entity.annotations.RootEntity;

// http://hannesdorfmann.com/annotation-processing/annotationprocessing101/

// https://github.com/pyricau/BuilderGen

public class EntityProcessor extends AbstractProcessor {

	public static String IMPL_SUFIX = "_Impl";

	public static String META_SUFIX = "_Meta";

	private final Map<TypeElement, JClass> implClasses = new HashMap<TypeElement, JClass>();

	public enum ObjectType {
		Entity, Primitive, List, Set;

		public static ObjectType getObjectType(String className) {
			if (IPrimitive.class.getName().equals(className)) {
				return Primitive;
			} else if (IList.class.getName().equals(className)) {
				return List;
			} else if (ISet.class.getName().equals(className)) {
				return Set;
			} else {
				return Entity;
			}
		}
	}

	public EntityProcessor() {
	}

	@Override
	public boolean process(Set<? extends TypeElement> elements, RoundEnvironment env) {
		for (Element element : env.getElementsAnnotatedWith(RootEntity.class)) {
			if (!implClasses.containsKey(element)) {
				generateImpl((TypeElement) element);
			}
		}

		if (env.processingOver()) {
			for (Element element : implClasses.keySet()) {
				generateMeta((TypeElement) element);
			}
			generateFactory();
		}
		return true;
	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		Set<String> annotataions = new LinkedHashSet<String>();
		annotataions.add(RootEntity.class.getCanonicalName());
		return annotataions;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.latestSupported();
	}

	private void error(Element element, String msg, Object... args) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String.format(msg, args), element);
	}

	private void note(String msg, Object... args) {
		processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, String.format(msg, args));
	}

	private void generateImpl(TypeElement entity) {
		try {
			EntityCodeWriter entityCodeWriter = new EntityCodeWriter(processingEnv.getFiler());

			JCodeModel codeModel = new JCodeModel();

			JDefinedClass entityImplClass = codeModel._class(entity.getQualifiedName() + IMPL_SUFIX);

			annotateGenerated(entityImplClass);

			if (entity.getKind() != ElementKind.INTERFACE) {
				error(entity, "Only interface can be annotated with @%s", RootEntity.class.getSimpleName());
				return;
			}

			if (entity.getInterfaces().size() != 1) {
				error(entity, "Only single inheritance is supported");
				return;
			}

			TypeMirror superInterfaceMirror = entity.getInterfaces().get(0);
			if (superInterfaceMirror.toString().equals(IEntity.class.getName())) {
				entityImplClass._extends(AbstractEntity.class);
			} else {
				entityImplClass._extends(getImplementationClass(asTypeElement(superInterfaceMirror)));
			}

			JClass elementClass = codeModel.ref(entity.getQualifiedName().toString());

			entityImplClass._implements(elementClass);

			{ // Constructor
				JMethod constructor = entityImplClass.constructor(JMod.PUBLIC);
				JVar pathVar = constructor.param(codeModel.ref(Path.class), "path");
				constructor.body().invoke("super").arg(pathVar);
			}

			JMethod getMemberMethod = entityImplClass.method(JMod.PUBLIC, codeModel.ref(IObject.class), "getMember");
			JVar stringParam = getMemberMethod.param(String.class, "string");
			JBlock getMemberMethodBody = getMemberMethod.body();
			JConditional getMemberMethodBodySwitch = null;

			for (Element element : entity.getEnclosedElements()) {
				if (element.getKind().equals(ElementKind.METHOD)) {
					ExecutableElement method = (ExecutableElement) element;

					String memberName = method.getSimpleName().toString();
					TypeMirror memberMirror = method.getReturnType();
					JClass typeArgument = null;

					note("Handling member %s", memberName);

					if (memberMirror.getKind() == TypeKind.DECLARED
							&& ((DeclaredType) memberMirror).getTypeArguments().size() == 1) {
						typeArgument = codeModel
								.ref(((DeclaredType) memberMirror).getTypeArguments().get(0).toString());
					}

					JFieldVar member = null;

					ObjectType objectType = ObjectType
							.getObjectType(asTypeElement(memberMirror).getQualifiedName().toString());

					switch (objectType) {
					case Primitive:
						member = entityImplClass.field(JMod.PRIVATE,
								codeModel.ref(PrimitiveImpl.class).narrow(typeArgument), memberName);
						break;
					case List:
						member = entityImplClass.field(JMod.PRIVATE, codeModel.ref(ListImpl.class).narrow(typeArgument),
								memberName);
						break;
					case Set:
						member = entityImplClass.field(JMod.PRIVATE, codeModel.ref(SetImpl.class).narrow(typeArgument),
								memberName);
						break;
					case Entity:
						member = entityImplClass.field(JMod.PRIVATE, codeModel.ref(memberMirror.toString()),
								memberName);
						break;
					}

					JType argType = codeModel.parseType(
							((ArrayType) method.getParameters().get(0).asType()).getComponentType().toString());

					JMethod memberMethod = null;

					{ // {member}() impl

						String memberMethodBuilder = null;

						switch (objectType) {
						case Primitive:
							memberMethod = entityImplClass.method(JMod.PUBLIC,
									codeModel.ref(PrimitiveImpl.class).narrow(typeArgument), memberName);
							memberMethodBuilder = "primitiveMember";
							break;
						case List:
							memberMethod = entityImplClass.method(JMod.PUBLIC,
									codeModel.ref(ListImpl.class).narrow(typeArgument), memberName);
							memberMethodBuilder = "listMember";
							break;
						case Set:
							memberMethod = entityImplClass.method(JMod.PUBLIC,
									codeModel.ref(SetImpl.class).narrow(typeArgument), memberName);
							memberMethodBuilder = "setMember";
							break;
						case Entity:
							memberMethod = entityImplClass.method(JMod.PUBLIC, codeModel.ref(memberMirror.toString()),
									memberName);
							memberMethodBuilder = "entityMember";
							break;
						}

						JVar value = memberMethod.varParam(argType,
								method.getParameters().get(0).getSimpleName().toString());
						JConditional pathCheck = memberMethod.body()._if(JExpr.invoke("getPath").eq(JExpr._null()));

						pathCheck._then().assign(member, JExpr.invoke(memberMethodBuilder).arg(member).arg(value))
								._return(member);

						JInvocation pathConstruct = JExpr._new(codeModel.ref(Path.class)).arg(JExpr._this())
								.arg(memberName);

						switch (objectType) {
						case Primitive:
							pathCheck._else()._return(JExpr
									._new(codeModel.ref(PrimitiveImpl.class).narrow(typeArgument)).arg(pathConstruct));
							break;
						case List:
							pathCheck._else()._return(
									JExpr._new(codeModel.ref(ListImpl.class).narrow(typeArgument)).arg(pathConstruct));
							break;
						case Set:
							pathCheck._else()._return(
									JExpr._new(codeModel.ref(SetImpl.class).narrow(typeArgument)).arg(pathConstruct));
							break;
						case Entity:
							pathCheck._else()
									._return(codeModel.ref(EntityFactory.class).staticInvoke("createPrototype")
											.arg(JExpr.dotclass(codeModel.ref(argType.fullName()))).arg(JExpr._this())
											.arg(memberName));
							break;
						}
					}

					{ // getMember() impl
						JExpression eqCheck = stringParam.eq(JExpr.lit(memberName));
						if (getMemberMethodBodySwitch == null) {
							getMemberMethodBodySwitch = getMemberMethodBody._if(eqCheck);
						} else {
							getMemberMethodBodySwitch = getMemberMethodBodySwitch._elseif(eqCheck);
						}
						getMemberMethodBodySwitch._then()
								._return(JExpr.invoke(memberMethod).arg(JExpr.cast(argType.array(), JExpr._null())));
					}
				}
			}

			getMemberMethodBodySwitch._else()._return(JExpr._super().invoke(getMemberMethod).arg(stringParam));

			codeModel.build(entityCodeWriter);

			implClasses.put(entity, entityImplClass);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (JClassAlreadyExistsException e) {
			e.printStackTrace();
		}

	}

	private void generateMeta(TypeElement entity) {

		try {
			EntityCodeWriter entityCodeWriter = new EntityCodeWriter(processingEnv.getFiler());

			JCodeModel codeModel = new JCodeModel();

			JDefinedClass entityImplClass = codeModel._class(entity.getQualifiedName() + META_SUFIX);

			annotateGenerated(entityImplClass);

			JClass elementClass = codeModel.ref(entity.getQualifiedName().toString());
			entityImplClass._extends(codeModel.ref(EntityMeta.class).narrow(elementClass));

			codeModel.build(entityCodeWriter);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JClassAlreadyExistsException e) {
			e.printStackTrace();
		}

	}

	private void generateFactory() {
		try {
			EntityCodeWriter entityCodeWriter = new EntityCodeWriter(processingEnv.getFiler());

			JCodeModel codeModel = new JCodeModel();

			JDefinedClass entityFactoryImplClass = codeModel._class(EntityFactory.class.getName() + IMPL_SUFIX);

			annotateGenerated(entityFactoryImplClass);

			entityFactoryImplClass._extends(AbstractEntityFactory.class);

			JMethod constructor = entityFactoryImplClass.constructor(JMod.PUBLIC);
			constructor.body().invoke("super");

			for (TypeElement element : implClasses.keySet()) {

				JClass entityClass = codeModel.ref(element.getQualifiedName().toString());
				JClass entityImplClass = codeModel.ref(entityClass.fullName() + IMPL_SUFIX);

				JDefinedClass entityInstantiatorClass = codeModel
						.anonymousClass(codeModel.ref(EntityInstantiator.class).narrow(entityClass));

				JMethod callMethod = entityInstantiatorClass.method(JMod.PUBLIC, entityClass, "newInstance");
				JVar pathParam = callMethod.param(codeModel.ref(Path.class), "path");
				callMethod.body()._return(JExpr._new(entityImplClass).arg(pathParam));

				constructor.body().invoke("addEntity").arg(JExpr.dotclass(entityClass))
						.arg(JExpr._new(entityInstantiatorClass))
						.arg(JExpr._new(codeModel.ref(element.getQualifiedName() + META_SUFIX)));
			}

			codeModel.build(entityCodeWriter);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JClassAlreadyExistsException e) {
			e.printStackTrace();
		}

	}

	private JClass getImplementationClass(TypeElement clazz) {
		if (!implClasses.containsKey(clazz)) {
			generateImpl(clazz);
		}
		return implClasses.get(clazz);
	}

	private TypeElement asTypeElement(TypeMirror typeMirror) {
		return (TypeElement) processingEnv.getTypeUtils().asElement(typeMirror);
	}

	private static void annotateGenerated(JDefinedClass entityImplClass) {
		entityImplClass.annotate(Generated.class) //
				.param("comments", "Generated by BuilderGen") //
				.param("value", EntityProcessor.class.getName()) //
				.param("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));

	}
}
