/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Nov 22, 2015
 * @author michaellif
 */
package ca.cellularautomata.core.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEntity extends AbstractObject implements IEntity {

	private Key key;

	public AbstractEntity() {
		super();
	}

	public AbstractEntity(Path path) {
		super(path);
	}

	@Override
	public Key getKey() {
		return key;
	}

	@Override
	public void setKey(Key key) {
		this.key = key;
	}

	@Override
	public IObject getMember(Path path) {
		List<Path> pathSegments = new ArrayList<>();
		do {
			pathSegments.add(0, path);
			if (path.getParent() != null) {
				path = path.getParent().getPath();
				// Exclude root segment
				if (path.getParent() == null) {
					path = null;
				}
			} else {
				path = null;
			}
		} while (path != null);

		IObject member = this;
		for (Path pathSegment : pathSegments) {
			if (member instanceof IEntity) {
				member = ((IEntity) member).getMember(pathSegment.getMemberName());
			} else {
				break;
			}
		}

		return member;
	}

	@Override
	public IObject getMember(String memberName) {
		throw new Error("Member " + memberName + "not found");
	}

	@SuppressWarnings("unchecked")
	protected <E extends Serializable> PrimitiveImpl<E> primitiveMember(IPrimitive<E> member, E... value) {
		if (member == null) {
			member = new PrimitiveImpl<E>(null);
		}
		if (value != null && value.length == 1) {
			try {
				member.setValue(value[0]);
			} catch (Exception e) {
				throw new Error(e);
			}

		}
		return (PrimitiveImpl<E>) member;
	}

	@SuppressWarnings("unchecked")
	protected <E extends IObject> ListImpl<E> listMember(IList<E> member, E... value) {
		if (member == null) {
			member = new ListImpl<E>(null);
		}
		if (value != null && value.length > 0) {
			member.clear();
			for (E item : value) {
				member.add(item);
			}
		}
		return (ListImpl<E>) member;
	}

	@SuppressWarnings("unchecked")
	protected <E extends IObject> SetImpl<E> setMember(ISet<E> member, E... value) {
		if (member == null) {
			member = new SetImpl<E>(null);
		}
		if (value != null && value.length > 0) {
			member.clear();
			for (E item : value) {
				member.add(item);
			}
		}
		return (SetImpl<E>) member;
	}

	@SuppressWarnings("unchecked")
	protected <E extends IObject> E entityMember(E member, E... value) {
		if (value != null && value.length == 1) {
			member = value[0];
		}
		return member;
	}

}
