/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Nov 22, 2015
 * @author michaellif
 */
package ca.cellularautomata.core.shared.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class AbstractEntityFactory {

	private final Map<Class<? extends IEntity>, EntityInstantiator<?>> instantiatorsMap = new HashMap<>();

	private final Map<Class<? extends IEntity>, EntityMeta<?>> entityMetaMap = new HashMap<>();

	protected AbstractEntityFactory() {
	}

	protected <T extends IEntity> void addEntity(Class<T> clazz, EntityInstantiator<T> entityInstantiator,
			EntityMeta<T> entityMeta) {
		instantiatorsMap.put(clazz, entityInstantiator);
		entityMetaMap.put(clazz, entityMeta);
	}

	public <T extends IEntity> T create(Class<T> clazz) {
		T inst = newInstance(clazz, null);
		return inst;
	}

	public <T extends IEntity, P extends IEntity> T createPrototype(Class<T> clazz, P parent, String memberName) {
		return newInstance(clazz, new Path(parent, memberName));
	}

	public <T extends IEntity> T createPrototype(Class<T> clazz) {
		return createPrototype(clazz, null, null);
	}

	@SuppressWarnings("unchecked")
	public <T extends IEntity> EntityMeta<T> getMeta(Class<T> clazz) {
		return (EntityMeta<T>) entityMetaMap.get(clazz);
	}

	@SuppressWarnings("unchecked")
	protected <T extends IEntity> T newInstance(Class<T> clazz, Path path) {
		assert (clazz != null) : "Can't create {null} class";
		EntityInstantiator<T> instantiator = (EntityInstantiator<T>) instantiatorsMap.get(clazz);
		if (instantiator == null) {
			throw new Error("Class " + clazz.getName() + " implementation not found");
		}

		T inst = instantiator.newInstance(path);
		inst.setKey(new Key(UUID.randomUUID()));
		return inst;
	}

	public abstract class EntityInstantiator<T extends IEntity> {
		protected abstract T newInstance(Path path);
	}
}