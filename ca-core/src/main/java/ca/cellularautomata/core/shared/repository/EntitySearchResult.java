/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Dec 17, 2015
 * @author michaellif
 */
package ca.cellularautomata.core.shared.repository;

import java.io.Serializable;
import java.util.ArrayList;

import ca.cellularautomata.core.shared.entity.IEntity;

public class EntitySearchResult<E extends IEntity> implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<E> data;

	private boolean hasMoreData;

	private String encodedCursorReference;

	private int totalRows = -1;

	public EntitySearchResult() {
		data = new ArrayList<E>();
	}

	public EntitySearchResult(ArrayList<E> data) {
		this.data = data;
	}

	public ArrayList<E> getData() {
		return data;
	}

	public void setData(ArrayList<E> data) {
		this.data = data;
	}

	public void add(E entity) {
		this.data.add(entity);
	}

	public boolean hasMoreData() {
		return hasMoreData;
	}

	public void hasMoreData(boolean hasMoreData) {
		this.hasMoreData = hasMoreData;
	}

	public int getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	public String getEncodedCursorReference() {
		return encodedCursorReference;
	}

	public void setEncodedCursorReference(String encodedCursorReference) {
		this.encodedCursorReference = encodedCursorReference;
	}

}