/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Nov 22, 2015
 * @author michaellif
 */
package ca.cellularautomata.core.shared.logger;

import java.util.logging.Level;

public class Logger {

    private java.util.logging.Logger javaLogger;

    public Logger(java.util.logging.Logger javaLogger) {
        this.javaLogger = javaLogger;
    }

    public void debug(String format, Object... args) {
        if (javaLogger.isLoggable(Level.FINE)) {
            javaLogger.log(Level.FINE, doFormat(format, args));
        }
    }

    public void info(String format, Object... args) {
        if (javaLogger.isLoggable(Level.INFO)) {
            javaLogger.log(Level.INFO, doFormat(format, args));
        }
    }

    public void warn(String format, Object... args) {
        if (javaLogger.isLoggable(Level.WARNING)) {
            javaLogger.log(Level.WARNING, doFormat(format, args));
        }
    }

    public void error(String format, Object... args) {
        if (javaLogger.isLoggable(Level.SEVERE)) {
            javaLogger.log(Level.SEVERE, doFormat(format, args));
        }
    }

    public void error(String message, Throwable t) {
        if (javaLogger.isLoggable(Level.SEVERE)) {
            javaLogger.log(Level.SEVERE, message, t);
        }
    }

    private static String doFormat(String format, Object[] args) {
        int i = 0;
        while (i < args.length) {
            String delimiter = "{" + i + "}";
            while (format.contains(delimiter)) {
                format = format.replace(delimiter, String.valueOf(args[i]));
            }
            i++;
        }
        return format;
    }
}
